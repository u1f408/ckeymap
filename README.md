# ckeymap

A Linux virtual console keymap parser, that works in no_std environments
(assuming alloc is available)

## License

MIT, see [LICENSE](./LICENSE)
